package ru.ilinovsg.tm.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractEntity {
    @Column(nullable = false, length = 50)
    private String name;

    public AbstractEntity(String name) {
        this.name = name;
    }
}
