package ru.ilinovsg.tm.repository;

import javax.persistence.NoResultException;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.ilinovsg.tm.config.HibernateConfig;
import ru.ilinovsg.tm.model.Chat;
import ru.ilinovsg.tm.model.Message;
import ru.ilinovsg.tm.model.User;

public class Repository {
    private static final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> void save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(entity);
        transaction.commit();
        session.close();
    }

    public <T> T getById(Class<T> entityClass, Serializable id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        T entity = session.get(entityClass, id);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> Optional<T> getByName(Class<T> entityClass, String name) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Optional<T> entity = Optional.empty();
        try {
            entity = Optional.ofNullable(session.createQuery("SELECT u FROM " + entityClass.getName() + " u where u.name = :name", entityClass)
                                                 .setParameter("name", name).getSingleResult());
        } catch (NoResultException e) {
        }
        transaction.commit();
        session.close();
        return entity;
    }

    public List<Message> getMessages(Chat chat, User currentUser) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Instant time = Instant.now();
        Instant time1 = time.minusSeconds(1);

        List<Message> messages = session.createQuery("SELECT m from Message m " +
                                                             "inner join m.user as c_user " +
                                                             "inner join m.chat as chat " +
                                                             "where c_user<>:currentUser and " +
                                                             "chat=:chat and " +
                                                             "m.time between :time1 and :time0", Message.class)
                .setParameter("currentUser", currentUser)
                .setParameter("chat", chat)
                .setParameter("time1", time1)
                .setParameter("time0", time)
                .getResultList();
        transaction.commit();
        session.close();
        return messages;
    }

    public List<User> getAllUsers() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        List<User> users = session.createQuery("SELECT u from User u", User.class).getResultList();
        transaction.commit();
        session.close();
        return users;
    }
}
