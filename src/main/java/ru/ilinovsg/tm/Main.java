package ru.ilinovsg.tm;

import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import ru.ilinovsg.tm.model.AbstractEntity;
import ru.ilinovsg.tm.model.Chat;
import ru.ilinovsg.tm.model.Message;
import ru.ilinovsg.tm.model.User;
import ru.ilinovsg.tm.repository.Repository;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static Repository repository = new Repository();
    private static String USER = "User";
    private static String CHAT = "Chat";
    private static String EXIT = "exit";

    public static void main(String[] args) {
        String command = "";
        User user = getEntity(USER, User.class);
        Chat chat = getEntity(CHAT, Chat.class);
        if(user.getUserId() == null){
            user.setChats(makeList(user.getChats(), chat));
            repository.save(user);
        }
        if(chat.getChatId() == null){
            chat.setUsers(makeList(chat.getUsers(), user));
            repository.save(chat);
        }
        startTimer(chat, user);
        while(command.toLowerCase(Locale.ROOT) != EXIT){
            command = scanner.nextLine();
            Message message = new Message();
            message.setMessage(command);
            message.setTime(Instant.now());
            message.setUser(user);
            message.setChat(chat);
            repository.save(message);
        }
    }

    private static <T extends AbstractEntity> List<T> makeList(List<T> existedList, T item){
        if (existedList == null) {
            return List.of(item);
        } else {
            if (!existedList.stream().anyMatch(chat1 -> chat1.getName().equals(item.getName()))) {
                existedList.add(item);
            }
        }
        return existedList;
    }

    private static <T extends AbstractEntity> T getEntity(String parameter, Class<T> entityClass) {
        System.out.println(parameter + ":");
        String name = scanner.nextLine();
        Optional<T> entityByName = repository.getByName(entityClass, name);
        if (entityByName.isPresent()) {
            return entityByName.get();
        } else {
            T newEntity = null;
            try {
                newEntity = entityClass.getConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
            newEntity.setName(name);
            return newEntity;
        }
    }

    private static void startTimer(Chat chat, User currentUser)  {
        Timer timer = new Timer();
        int begin = 0;
        int timeInterval = 1000;
        timer.schedule(new TimerTask() {
            int counter = 0;
            @Override
            public void run() {
                repository.getMessages(chat,currentUser).forEach(message -> {
                    System.out.println(message.getTime()+" "+message.getUser().getName() + " :"+message.getMessage());
                });
                counter++;
            }
        }, begin, timeInterval);
    }
}
